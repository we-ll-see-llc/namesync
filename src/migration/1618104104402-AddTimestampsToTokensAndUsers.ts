import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AddTimestampsToTokensAndUsers1618104104402
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      "users",
      new TableColumn({
        name: "created_at",
        type: "timestamp",
        default: "now()",
      })
    );

    await queryRunner.addColumn(
      "tokens",
      new TableColumn({
        name: "created_at",
        type: "timestamp",
        default: "now()",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn("users", "created_at");
    await queryRunner.dropColumn("tokens", "created_at");
  }
}
