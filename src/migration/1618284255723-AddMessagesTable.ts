import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from "typeorm";

export class AddMessagesTable1618284255723 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "messages",
        columns: [
          {
            name: "id",
            type: "text",
            isPrimary: true,
          },
          {
            name: "content",
            type: "text",
          },
          {
            name: "recipient_service_id",
            type: "text",
          },
          {
            name: "recipient_service",
            type: "text",
          },
          {
            name: "created_by_token",
            type: "text",
          },
          {
            name: "delivered_at",
            type: "timestamp",
            isNullable: true,
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
        ],
      })
    );

    await queryRunner.createForeignKey(
      "messages",
      new TableForeignKey({
        referencedTableName: "tokens",
        columnNames: ["created_by_token"],
        referencedColumnNames: ["token"],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("messages");
  }
}
