import { MigrationInterface, QueryRunner } from "typeorm";
import { query } from "express";

export class CreateMappingsAutoIncrement1615522252767
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query("CREATE SEQUENCE mapping_id_seq");
    await queryRunner.query(
      "ALTER TABLE mappings ALTER id SET DEFAULT nextval('mapping_id_seq')"
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE mappings ALTER COLUMN id DROP DEFAULT `
    );
    await queryRunner.query(`DROP SEQUENCE mapping_id_seq`);
  }
}
