import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateInstallationTable1615347620060
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "installations",
        columns: [
          {
            name: "id",
            type: "text",
            isPrimary: true,
          },
          {
            name: "installation",
            type: "json",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("installations");
  }
}
