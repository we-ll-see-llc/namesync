import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from "typeorm";

export class CreateTokensTable1618096217694 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "tokens",
        columns: [
          {
            name: "token",
            type: "text",
            isPrimary: true,
          },
          {
            name: "user_id",
            type: "text",
          },
          {
            name: "installation_id",
            type: "text",
          },
        ],
      })
    );

    await queryRunner.createTable(
      new Table({
        name: "users",
        columns: [
          {
            name: "user_id",
            type: "text",
            isPrimary: true,
          },
          {
            name: "installation_id",
            type: "text",
            isPrimary: true,
          },
          {
            name: "email",
            type: "text",
          },
        ],
      })
    );

    await queryRunner.createForeignKey(
      "tokens",
      new TableForeignKey({
        referencedTableName: "users",
        columnNames: ["user_id", "installation_id"],
        referencedColumnNames: ["user_id", "installation_id"],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("tokens");
    await queryRunner.dropTable("users");
  }
}
