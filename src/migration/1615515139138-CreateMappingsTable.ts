import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableUnique,
} from "typeorm";

export class CreateMappingsTable1615515139138 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "mappings",
        columns: [
          {
            name: "id",
            type: "bigint",
            isPrimary: true,
          },
          {
            name: "installation_id",
            type: "text",
          },
          {
            name: "slack_id",
            type: "text",
          },
          {
            name: "service_id",
            type: "text",
          },
          {
            name: "service",
            type: "text",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
        ],
      })
    );

    await queryRunner.createForeignKey(
      "mappings",
      new TableForeignKey({
        columnNames: ["installation_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "installations",
      })
    );

    await queryRunner.createUniqueConstraint(
      "mappings",
      new TableUnique({
        columnNames: ["service", "installation_id", "service_id"],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("mappings");
  }
}
