import { Entity, Column, PrimaryColumn, OneToMany } from "typeorm";
import * as Slack from "@slack/bolt";
import { Mapping } from "./Mapping";

@Entity({ name: "installations" })
export class Installation {
  @PrimaryColumn()
  id: string;

  @Column({ type: "jsonb" })
  installation: Slack.Installation;

  @OneToMany(() => Mapping, (mapping) => mapping.installation)
  mappings: Mapping[];
}
