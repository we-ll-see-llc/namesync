import {
  Entity,
  Column,
  PrimaryColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from "typeorm";
import { Installation } from "./Installations";
import { User } from "./User";
import { Token } from "./Token";

@Entity({ name: "messages" })
export class Message {
  @PrimaryColumn()
  id: string;

  @Column()
  content: string;

  @Column({ name: "recipient_service_id" })
  recipientServiceId: string;

  @Column({ name: "recipient_service" })
  recipientService: string;

  @Column({ name: "created_by_token" })
  createdByToken: string;

  @Column({ name: "delivered_at", nullable: true })
  deliveredAt?: Date;

  @ManyToOne(() => Token, (token) => token.messages)
  @JoinColumn([{ name: "created_by_token" }])
  createdBy: Token;
}
