import {
  Entity,
  Column,
  PrimaryColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from "typeorm";
import { Installation } from "./Installations";
import { User } from "./User";
import { Message } from "./Message";

@Entity({ name: "tokens" })
export class Token {
  @PrimaryColumn()
  token: String;

  @Column({ name: "user_id" })
  userId: string;

  @Column({ name: "installation_id" })
  installationId: string;

  @ManyToOne(() => User, (user) => user.tokens)
  @JoinColumn([
    { name: "installation_id", referencedColumnName: "installationId" },
    { name: "user_id", referencedColumnName: "userId" },
  ])
  user: User;

  @OneToMany(() => Message, (message) => message.createdBy)
  messages: Message[];
}
