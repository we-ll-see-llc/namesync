import { Entity, Column, PrimaryColumn, OneToMany } from "typeorm";
import { Token } from "./Token";

@Entity({ name: "users" })
export class User {
  @PrimaryColumn({ name: "user_id" })
  userId: string;

  @PrimaryColumn({ name: "installation_id" })
  installationId: string;

  @Column()
  email: string;

  @OneToMany(() => Token, (token) => token.user)
  tokens: Token[];
}
