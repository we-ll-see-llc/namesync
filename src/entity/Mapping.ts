import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Unique,
} from "typeorm";
import { Installation } from "./Installations";

@Entity({ name: "mappings" })
@Unique(["service", "slackId"])
export class Mapping {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ name: "service_id" })
  serviceId: string;

  @Column()
  service: string;

  @Column({ name: "slack_id" })
  slackId: string;

  @ManyToOne(() => Installation, (installation) => installation.mappings)
  @JoinColumn({ name: "installation_id" })
  installation: Installation;
}
