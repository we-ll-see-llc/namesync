import { App, ExpressReceiver } from "@slack/bolt";
import passport from "passport";
import { Strategy as GitHubStrategy } from "passport-github";
import "reflect-metadata";
import dotenv from "dotenv";
import {
  createConnection,
  getConnection,
  getRepository,
  IsNull,
} from "typeorm";
import { Installation } from "./entity/Installations";
import { Mapping } from "./entity/Mapping";
import sassMiddleware from "node-sass-middleware";
import express from "express";
import session from "express-session";
import bodyParser from "body-parser";
// @ts-ignore
import { Strategy as SlackStrategy } from "passport-slack-oauth2";
import { Strategy as BearerStrategy } from "passport-http-bearer";
import { User } from "./entity/User";
import { Token } from "./entity/Token";
import { v4 as uuidv4 } from "uuid";
import { Message } from "./entity/Message";
import { Octokit } from "octokit";
import asyncHandler from "express-async-handler";

dotenv.config();

createConnection().then(async (connection) => {
  let repo = connection.getRepository(Mapping);
  const installations = await repo.find();
  console.log(installations);
});

passport.use(
  new GitHubStrategy(
    {
      clientID: process.env.GITHUB_APP_CLIENT_ID!,
      clientSecret: process.env.GITHUB_APP_CLIENT_SECRENT!,
      callbackURL: `${process.env.BASE_URL}/auth/github/callback`,
    },
    (accessToken, refreshToken, profile, cb) => {
      cb(undefined, { profile: profile });
    }
  )
);

passport.use(
  new SlackStrategy(
    {
      clientID: process.env.SLACK_CLIENT_ID,
      clientSecret: process.env.SLACK_CLIENT_SECRET,
      skipUserProfile: false, // default
      callbackURL: `${process.env.BASE_URL}/auth/slack/callback`,
      scope: [
        "identity.basic",
        "identity.email",
        "identity.avatar",
        "identity.team",
      ], //
    },
    (accessToken: any, refreshToken: any, profile: any, done: any) => {
      // optionally persist user data into a database
      done(null, profile);
    }
  )
);

passport.use(
  new BearerStrategy(async (tokenKey, done) => {
    try {
      const token = await getConnection()
        .getRepository(Token)
        .findOneOrFail(
          {
            token: tokenKey,
          },
          { relations: ["user"] }
        );

      console.log("user", token.user);
      return done(null, token);
    } catch {
      return done(null, false);
    }
  })
);

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user as any);
});

const receiver = new ExpressReceiver({
  signingSecret: process.env.SLACK_SIGNING_SECRET!,
  clientId: process.env.SLACK_CLIENT_ID,
  clientSecret: process.env.SLACK_CLIENT_SECRET,
  stateSecret: "my-state-secret",
  scopes: ["channels:history", "chat:write", "im:write"],
  installationStore: {
    storeInstallation: async (installation) => {
      // change the line below so it saves to your database
      console.log(`installing ${installation}`);

      if (installation.isEnterpriseInstall) {
        // support for org wide app installation
        await getConnection().manager.getRepository(Installation).save({
          id: installation.enterprise!.id!,
          installation,
        });
      } else {
        // single team app installation
        await getConnection().manager.getRepository(Installation).save({
          id: installation.team!.id,
          installation,
        });
      }
    },
    fetchInstallation: async (installQuery) => {
      if (
        installQuery.isEnterpriseInstall &&
        installQuery.enterpriseId !== undefined
      ) {
        // org wide app installation lookup
        return await getConnection()
          .manager.getRepository(Installation)
          .findOne({ id: installQuery.enterpriseId })
          .then((i) => i!.installation);
      }

      if (installQuery.teamId !== undefined) {
        // single team app installation lookup
        return await getConnection()
          .manager.getRepository(Installation)
          .findOne({ id: installQuery.teamId })
          .then((i) => i!.installation);
      }

      throw new Error("Failed fetching installation");
    },
  },
});

receiver.app.set("views", "views");
receiver.app.set("view engine", "pug");
receiver.app.use(
  sassMiddleware({
    src: "sass", //where the sass files are
    dest: "public", //where css should go
    prefix: "/assets",
  })
);
receiver.app.use("/assets", express.static("public"));

const slackApp = new App({
  receiver,
});

slackApp.action("thats_me_click", async (args) => {
  // Acknowledge the action
  await args.ack();

  await args.client.chat.postEphemeral({
    channel: args.body.channel?.id!,
    text: "Link your github account",
    blocks: [
      {
        type: "actions",
        elements: [
          {
            type: "button",
            action_id: "link_github_click",
            url: `${process.env.BASE_URL}/auth/github?userId=${
              args.body.user.id
            }&channelId=${args.body.channel!.id}&teamId=${args.body.team?.id!}`,
            text: {
              type: "plain_text",
              text: `Link with github account`,
            },
          },
        ],
      },
    ],
    user: args.body.user.id,
  });
});

slackApp.action("link_github_click", async ({ ack }) => {
  await ack();
});

(async () => {
  // Start your app
  await slackApp.start(parseInt(process.env.PORT!));

  console.log(`⚡️ Bolt app is running at ${process.env.BASE_URL}`);
})();

receiver.router.use(bodyParser.json());
receiver.router.use(session({ secret: "cats" }));
receiver.router.use(passport.initialize());
receiver.router.use(passport.session());

receiver.router.get("/auth/github", function (req, res, next) {
  const authenticator = passport.authenticate("github", {
    state: new Buffer(
      JSON.stringify({
        userId: req.query.userId,
        channelId: req.query.channelId,
        teamId: req.query.teamId,
      })
    ).toString("base64"),
  });

  authenticator(req, res, next);
});

receiver.router.get(
  "/auth/github/callback",
  passport.authenticate("github", {
    failureRedirect: "/login",
    session: false,
  }),
  asyncHandler(async (req, res) => {
    const { state: stateString } = req.query;
    const state = JSON.parse(
      new Buffer(stateString as string, "base64").toString()
    );
    console.log("state", state);
    console.log(req.user);
    const auth = await receiver.installer?.authorize({
      teamId: state.teamId,
      isEnterpriseInstall: false,
      enterpriseId: undefined,
    });

    const installation = await getConnection()
      .getRepository(Installation)
      .findOne({ id: state.teamId });

    const existingAccount = await getConnection()
      .getRepository(Mapping)
      .findOne({
        service: "github",
        serviceId: (req.user as any).profile.id,
        installation: installation,
      });

    if (!existingAccount) {
      await getConnection()
        .getRepository(Mapping)
        .insert({
          service: "github",
          serviceId: (req.user as any).profile.id,
          installation: installation,
          slackId: state.userId,
        });
    }

    slackApp.client.chat.postMessage({
      token: auth!.botToken,
      text: `Success, <@${state.userId}> is now linked to github account ${
        (req.user as any).profile.username
      }`,
      channel: state.channelId,
    });

    const messages = await getConnection()
      .getRepository(Message)
      .find({
        recipientService: "github",
        recipientServiceId: (req.user as any).profile.id,
        deliveredAt: IsNull(),
      });

    for (const message of messages) {
      await slackApp.client.chat.postMessage({
        token: auth!.botToken,
        text: message.content,
        channel: state.userId,
      });

      await getRepository(Message).update(message.id, {
        deliveredAt: new Date(),
      });
    }

    // Successful authentication, redirect home.
    res.redirect("/");
  })
);

receiver.router.get("/", async (req, res) => {
  const installURL = await receiver.installer?.generateInstallUrl({
    redirectUri: `${process.env.BASE_URL}/slack/post_install`,
    scopes: ["chat:write", "im:write"],
  });

  res.render("index", {
    installURL,
    justInstalled: req.query.justInstalled,
  });
});

receiver.router.get("/token", async (req, res) => {
  if (!req.user) {
    res.redirect("auth/slack");
  } else {
    const slackUser = req.user as any;

    console.log("user", req.user);

    const userId: string = slackUser.user.id;
    const installationId = slackUser.team.id;

    let user: User;
    const existingUser = await getConnection()
      .getRepository(User)
      .findOne({ userId, installationId });

    if (!existingUser) {
      user = getConnection()
        .getRepository(User)
        .create({
          userId,
          installationId,
          email: (req.user as any).user.email,
        });
      await getConnection().getRepository(User).insert(user);
    } else {
      user = existingUser;
    }

    const token = uuidv4().toString();
    await getConnection().getRepository(Token).insert({
      user,
      token,
    });

    const installURL = await receiver.installer?.generateInstallUrl({
      redirectUri: `${process.env.BASE_URL}/slack/post_install`,
      scopes: ["chat:write", "im:write"],
    });

    res.render("index", {
      installURL,
      token,
    });
  }
});

receiver.router.get(
  "/auth/slack",
  passport.authenticate("Slack", { failureRedirect: "/login" })
);

receiver.router.get(
  "/auth/slack/callback",
  passport.authenticate("Slack", { failureRedirect: "/login" }),
  (req, res) => {
    res.redirect("/token");
  }
);

receiver.router.get("/slack/post_install", (req, res) => {
  receiver.installer?.handleCallback(req, res, {
    success: (installation, metadata, req_, res_) => {
      res.redirect("/?justInstalled=true");
    },
  });
});

receiver.router.get(
  "/api/github/:githubId",
  passport.authenticate("bearer", { session: false }),
  asyncHandler(async (req, res) => {
    const token: Token = req.user as Token;

    const installation = await getConnection()
      .getRepository(Installation)
      .findOne({
        id: token.installationId,
      });

    if (!installation) {
      res.statusCode = 400;
      return res.json({
        error: "Haven't added the thatsme.dev app to your Slack yet",
      });
    }

    const mapping = await getConnection().getRepository(Mapping).findOne({
      service: "github",
      serviceId: req.params.githubId,
      installation,
    });

    if (!mapping) {
      res.statusCode = 404;
      return res.json({
        error: "Unmapped",
      });
    }

    res.json({ slackId: mapping.slackId });
  })
);

receiver.router.post(
  "/api/github/:githubId/messages",
  passport.authenticate("bearer", { session: false }),
  asyncHandler(async (req, res) => {
    const token: Token = req.user as Token;

    const installation = await getConnection()
      .getRepository(Installation)
      .findOne({
        id: token.installationId,
      });

    if (!installation) {
      res.statusCode = 400;
      return res.json({
        error: "Haven't added the thatsme.dev app to your Slack yet",
      });
    }

    const auth = await receiver.installer?.authorize({
      teamId: installation.id,
      isEnterpriseInstall: false,
      enterpriseId: undefined,
    });

    const mapping = await getConnection().getRepository(Mapping).findOne({
      service: "github",
      serviceId: req.params.githubId,
      installation,
    });

    if (!req.body.message || !req.body.searchChannel) {
      res.statusCode = 400;
      return res.json({ error: "missing required fields" });
    }

    const message = getConnection().getRepository(Message).create({
      id: uuidv4(),
      createdBy: token,
      recipientService: "github",
      recipientServiceId: req.params.githubId,
      content: req.body.message,
    });

    if (mapping) {
      res.statusCode = 201;
      await slackApp.client.chat.postMessage({
        token: auth!.botToken,
        text: req.body.message,
        channel: mapping.slackId,
      });

      message.deliveredAt = new Date();
    } else {
      const ghResponse = await new Octokit().request("GET /user/:id", {
        id: req.params.githubId,
      });

      const githubName = ghResponse.data.login;
      let text = `I have a message to deliver to the GitHub user ${githubName}`;

      try {
        await slackApp.client.chat.postMessage({
          token: auth!.botToken,
          channel: req.body.searchChannel,
          text: text,
          blocks: [
            {
              type: "section",
              text: {
                type: "plain_text",
                text: text,
              },
            },
            {
              type: "actions",
              elements: [
                {
                  type: "button",
                  action_id: "thats_me_click",
                  text: {
                    type: "plain_text",
                    text: "That's me",
                  },
                },
              ],
            },
          ],
        });
        res.statusCode = 202;
      } catch (e) {
        if (e.data.error === "not_in_channel") {
          res.statusCode = 400;
          return res.json({
            error: `thatsme.dev bot is unable to post in ${req.body.searchChannel} without first being invited`,
          });
        } else {
          throw e;
        }
      }
    }

    await getConnection().getRepository(Message).insert(message);

    res.json({});
  })
);

receiver.router.use([
  (err, req, res, next) => {
    res.status(500);
    console.error(err);
    res.json({ error: "an unknown error occurred" });
  },
]);
