module.exports = {
  type: "postgres",
  url: process.env.DATABASE_URL,
  extra: { ssl: { rejectUnauthorized: false } },
  logging: false,
  entities: ["bin/src/entity/**/*.js"],
  migrations: ["bin/src/migration/**/*.js"],
  subscribers: ["src/subscriber/**/*.ts"],
  cli: {
    entitiesDir: "src/entity",
    migrationsDir: "src/migration",
    subscribersDir: "src/subscriber",
  },
};
